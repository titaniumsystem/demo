import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { strictEqual } from 'assert';
import { stringify } from '@angular/core/src/render3/util';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private router: Router) {

}

    // Este servicio se encarga de verificar las credenciales y de si estamos logueados o no.
    verificaLogin(usuario, passwd): boolean {
      // Peticion a api imaginaria
      if (usuario === 'admin@admin.es' && passwd === 'cohosting') {

        localStorage.setItem('loginToken', 'tokenValido');

      }
      return usuario === 'admin@admin.es' && passwd === 'cohosting';
    }

    amILogged(): boolean {
      let token: string;
      token = localStorage.getItem('loginToken');
      return token === 'tokenValido';
   }

   logout(): void {
     localStorage.removeItem('loginToken');
     this.router.navigate(['/login']);

   }

}
