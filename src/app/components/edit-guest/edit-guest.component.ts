import { GuestsService } from './../../services/guests.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Invitado } from 'src/app/services/guests.service';

@Component({
  selector: 'app-edit-guest',
  templateUrl: './edit-guest.component.html',
  styleUrls: ['./edit-guest.component.less']
})
export class EditGuestComponent implements OnInit {

  invitado: Invitado;

  constructor( private activatedRoute: ActivatedRoute,
               private servicioInvitados: GuestsService,
               private router: Router ) {
    this.activatedRoute.params.subscribe( params => {
      const key = 'id'; // typescript a veces se pone pesado con algunas cosas
      const id = params[key];
      if ( this.inRange (id)) {
      this.invitado = servicioInvitados.getInvitados()[id];
      } else {
        // Si no es un id válido volvemos a la lista
        router.navigate(['/listaInvitados']);
      }
    });


   }

  ngOnInit() {
  }

  updateInvitado() {
    this.servicioInvitados.editGuest(this.invitado);
  }

  private inRange(id: number) {
    return id >= 0 && id < this.servicioInvitados.getInvitados().length;
  }

}
