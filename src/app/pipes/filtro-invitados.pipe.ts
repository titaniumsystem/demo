import { Pipe, PipeTransform } from '@angular/core';
import { Invitado } from '../services/guests.service';

@Pipe({
  name: 'filtroInvitados'
})
export class FiltroInvitadosPipe implements PipeTransform {

  transform(invitados: Invitado[], busqueda?: string): Invitado[] {
    // Filtramos los invitados en los que el nombre contenga la cadena busqueda
    // Primero: si no hay nada en busqueda o es una cadena en blanco, devuelve la lista completa
    if (busqueda === null || busqueda.trim() === '') {
      // Se que no es una buena práctica varios returns, pero el hecho de tener que hacer
      // una copia solo para tener que devolverla igual me parecía innecesario
      return invitados;
    }

    let res: Invitado[] = [];

    invitados.forEach(element => {
      // Si contiene en el nombre el parámetro de la busqueda
      if ( element.nombre.toLowerCase().indexOf(busqueda.toLocaleLowerCase()) !== -1) {
        // lo añadimos a res
        res.push(element);

      }
    });

    return res;
  }

}
